package Insurance.Claiming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaimingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimingApplication.class, args);
	}

}
