package Insurance.Claiming.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table
@NoArgsConstructor
@Getter
@Setter
public class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    @Column(name = "premimum_amount")
    private int premimumAmount;
    private String company;
    @Column(name = "claim_amount")
    private int claimAmount;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    @ManyToOne
    @JoinColumn(name = "subCategory_id")
    private SubCategory subCategory;

    @ManyToOne
    @JoinColumn(name = "insurancePaymentType_id")
    private InsurancePaymentType insurancePaymentType;

}