package Insurance.Claiming.entities;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table
public class InsurancePaymentType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "insurance_id")
    private Insurance insurance;
    @ManyToOne
    @JoinColumn(name = "paymentType_id")
    private PaymentType paymentType;
}
